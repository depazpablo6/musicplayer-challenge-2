package com.applaudostudios.musicplayer

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity
import com.applaudostudios.musicplayer.databinding.ActivityMainBinding
import com.applaudostudios.musicplayer.service.MediaPlayerService

class MainActivity : AppCompatActivity(), ServiceConnection {

    companion object {
        const val MILLISECONDS = 1000
        const val SECONDS = 60
    }

    private lateinit var binding: ActivityMainBinding
    private val updateHandler: Handler = Handler(Looper.getMainLooper())
    private lateinit var runnable: Runnable
    private var mediaPlayerService: MediaPlayerService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        startMediaService()
        setListeners()
    }

    private fun setSongInfo() {
        val image: ByteArray? = mediaPlayerService?.song?.image
        image?.let {
            binding.musicImage.setImageBitmap(BitmapFactory.decodeByteArray(image, 0, image.size))
        } ?: binding.musicImage.setImageResource(R.drawable.mainicon)
        binding.songName.text = mediaPlayerService?.song?.songName
        binding.artist.text = mediaPlayerService?.song?.artist
    }

    private fun setListeners() {

        binding.playpauseButton.setOnClickListener {
            if (mediaPlayerService?.isPlaying() == true) {
                binding.playpauseButton.setBackgroundResource(R.drawable.playbutton)
                mediaPlayerService?.pauseMediaPlayer()
                mediaPlayerService?.showNotification(R.drawable.playbutton)
            } else {
                binding.playpauseButton.setBackgroundResource(R.drawable.pausebutton)
                mediaPlayerService?.startMediaPlayer()
                mediaPlayerService?.showNotification(R.drawable.pausebutton)
            }
        }

        binding.infoButton.setOnClickListener {
            val fragment = InformationFragment()
            fragment.arguments = mediaPlayerService?.bundle
            val ft = supportFragmentManager.beginTransaction()
            ft.replace(R.id.fragment_container, fragment)
            ft.commit()
        }

        binding.nextButton.setOnClickListener {
            binding.playpauseButton.setBackgroundResource(R.drawable.pausebutton)
            mediaPlayerService?.nextSong()
            setSongInfo()
        }

        binding.backButton.setOnClickListener {
            binding.playpauseButton.setBackgroundResource(R.drawable.pausebutton)
            mediaPlayerService?.previousSong()
            setSongInfo()
        }


    }

    private fun startMediaService() {
        val serviceIntent = Intent(applicationContext, MediaPlayerService::class.java)
        startService(serviceIntent)
    }


    private fun seekBarInitializer() {
        var min: Int
        var sec: Int
        var remainingTime: Int
        runnable = Runnable {
            val duration = mediaPlayerService?.getSongDuration() ?: 0
            val currentPosition = mediaPlayerService?.getCurrentPosition() ?: 0
            binding.progressBar.max = duration
            binding.progressBar.progress = currentPosition
            min = binding.progressBar.progress.div(MILLISECONDS).div(SECONDS)
            sec = binding.progressBar.progress.div(MILLISECONDS).rem(SECONDS)
            val currentTime = "$min:${if (sec < 10) "0$sec" else sec}"
            binding.currentTime.text = currentTime
            remainingTime = duration - currentPosition
            min = remainingTime.div(1000).div(60)
            sec = remainingTime.div(1000).rem(60)
            val rTime = "${min}:${if (sec < 10) "0$sec" else sec}"
            binding.remainingTime.text = rTime
            updateHandler.postDelayed(runnable, 1000)
        }
        updateHandler.postDelayed(runnable, 0)

        binding.progressBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                if (p2) mediaPlayerService?.seekTo(p1)
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

        })
    }

    override fun onResume() {
        val intent = Intent(applicationContext, MediaPlayerService::class.java)
        bindService(intent, this, BIND_AUTO_CREATE)
        super.onResume()
    }

    override fun onPause() {
        unbindService(this)
        super.onPause()
    }

    override fun onServiceConnected(p0: ComponentName?, p1: IBinder?) {
        val mediaBinder = p1 as MediaPlayerService.MediaPlayerBinder
        mediaPlayerService = mediaBinder.getService()
        setSongInfo()
        seekBarInitializer()
        mediaPlayerService?.playing?.observe(this, {
            if (mediaPlayerService?.isPlaying() == true) {
                binding.playpauseButton.setBackgroundResource(R.drawable.pausebutton)
            } else {
                binding.playpauseButton.setBackgroundResource(R.drawable.playbutton)
            }
        })
        mediaPlayerService?.nextSong?.observe(this, {
            setSongInfo()
            isPlaying()
        })
        isPlaying()
    }

    private fun isPlaying() {
        if (mediaPlayerService?.isPlaying() == true) {
            binding.playpauseButton.setBackgroundResource(R.drawable.pausebutton)
            mediaPlayerService?.showNotification(R.drawable.pausebutton)
        } else {
            binding.playpauseButton.setBackgroundResource(R.drawable.playbutton)
            mediaPlayerService?.showNotification(R.drawable.playbutton)
        }
    }

    override fun onServiceDisconnected(p0: ComponentName?) {
        mediaPlayerService = null
    }

}