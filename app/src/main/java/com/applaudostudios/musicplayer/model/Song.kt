package com.applaudostudios.musicplayer.model

class Song(
    val songName: String?,
    val artist: String?,
    val year: String?,
    val album: String?,
    val track: String?,
    val image: ByteArray?
)