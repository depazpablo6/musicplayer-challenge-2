package com.applaudostudios.musicplayer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.applaudostudios.musicplayer.databinding.FragmentInformationBinding

class InformationFragment : Fragment() {

    private var _binding: FragmentInformationBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentInformationBinding.inflate(inflater, container, false)
        binding.infoSongName.text = arguments?.getString("songName")
        binding.infoArtist.text = arguments?.getString("artist")
        binding.infoYear.text = arguments?.getString("year")
        binding.infoAlbum.text = arguments?.getString("album")
        binding.infoTrack.text = arguments?.getString("track")
        binding.infoDuration.text = arguments?.getString("duration")
        binding.infoBackground.setOnClickListener {
            fragmentManager?.beginTransaction()?.remove(this)?.commit()
        }
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}