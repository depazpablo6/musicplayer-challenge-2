package com.applaudostudios.musicplayer.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.applaudostudios.musicplayer.service.App.Companion.ACTION_NEXT
import com.applaudostudios.musicplayer.service.App.Companion.ACTION_PLAY
import com.applaudostudios.musicplayer.service.App.Companion.ACTION_PREVIOUS
import com.applaudostudios.musicplayer.service.MediaPlayerService

class MediaPlayerReceiver : BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent?) {
        val actionIntent = Intent(p0, MediaPlayerService::class.java)
        when (p1?.action) {
            ACTION_PLAY -> {
                actionIntent.putExtra("action", ACTION_PLAY)
                p0?.startService(actionIntent)
            }
            ACTION_NEXT -> {
                actionIntent.putExtra("action", ACTION_NEXT)
                p0?.startService(actionIntent)
            }
            ACTION_PREVIOUS -> {
                actionIntent.putExtra("action", ACTION_PREVIOUS)
                p0?.startService(actionIntent)
            }
        }
    }
}