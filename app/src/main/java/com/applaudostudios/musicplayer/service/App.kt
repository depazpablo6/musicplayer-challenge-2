package com.applaudostudios.musicplayer.service

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build

class App : Application() {

    companion object {
        const val CHANNEL_ID = "playerServiceChannel"
        const val CHANNEL_ID2 = "playerServiceChannel2"
        const val ACTION_PLAY = "actionPlay"
        const val ACTION_NEXT = "actionNext"
        const val ACTION_PREVIOUS = "actionPrevious"
    }

    override fun onCreate() {
        super.onCreate()
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val playerChannel: NotificationChannel = NotificationChannel(
                CHANNEL_ID,
                "Media Player Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val playerChannel2: NotificationChannel = NotificationChannel(
                CHANNEL_ID2,
                "Media Player Channel 2",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager: NotificationManager =
                getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            manager.createNotificationChannel(playerChannel)
            manager.createNotificationChannel(playerChannel2)

        }
    }

}