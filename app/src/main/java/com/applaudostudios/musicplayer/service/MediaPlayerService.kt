package com.applaudostudios.musicplayer.service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.os.Binder
import android.os.Bundle
import android.os.IBinder
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import androidx.core.app.NotificationCompat
import androidx.lifecycle.MutableLiveData
import androidx.media.session.MediaButtonReceiver
import com.applaudostudios.musicplayer.MainActivity
import com.applaudostudios.musicplayer.R
import com.applaudostudios.musicplayer.model.Song
import com.applaudostudios.musicplayer.receiver.MediaPlayerReceiver
import com.applaudostudios.musicplayer.service.App.Companion.ACTION_NEXT
import com.applaudostudios.musicplayer.service.App.Companion.ACTION_PLAY
import com.applaudostudios.musicplayer.service.App.Companion.ACTION_PREVIOUS
import com.applaudostudios.musicplayer.service.App.Companion.CHANNEL_ID

class MediaPlayerService : Service() {

    private val mediaBinder = MediaPlayerBinder()
    private lateinit var mediaPlayer: MediaPlayer
    private var songs = mutableListOf(R.raw.besomebody, R.raw.jumpupsuperstar, R.raw.lostinthoughts)
    private var actualSong = songs[0]
    var song: Song? = null
    private var mmr: MediaMetadataRetriever = MediaMetadataRetriever()
    lateinit var bundle: Bundle
    var nextSong = MutableLiveData<Int>()
    var playing = MutableLiveData<Boolean>()
    private var initialized = false
    private lateinit var token: MediaSessionCompat

    inner class MediaPlayerBinder : Binder() {
        fun getService(): MediaPlayerService = this@MediaPlayerService
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!initialized) {
            createMediaPlayer()
            setMetadataSong(actualSong)
            setCompleteListener()
            initialized = true
            nextSong.value = actualSong
            playing.value = false
            token = MediaSessionCompat(applicationContext, "MediaPlayer")
        }

        when (intent?.getStringExtra("action")) {
            ACTION_PLAY -> {
                if (mediaPlayer.isPlaying) {
                    mediaPlayer.pause()
                    showNotification(R.drawable.playbutton)
                    playing.value = false
                } else {
                    mediaPlayer.start()
                    showNotification(R.drawable.pausebutton)
                    playing.value = true
                }
            }
            ACTION_PREVIOUS -> {
                previousSong()
            }
            ACTION_NEXT -> {
                nextSong()
            }
        }

        return START_NOT_STICKY
    }

    override fun onBind(p0: Intent?): IBinder = mediaBinder

    private fun createMediaPlayer() =
        run { mediaPlayer = MediaPlayer.create(applicationContext, actualSong) }

    fun startMediaPlayer() = mediaPlayer.start()

    fun pauseMediaPlayer() = mediaPlayer.pause()

    private fun stopMediaPlayer() = mediaPlayer.stop()

    private fun releaseMediaPlayer() = mediaPlayer.release()

    fun isPlaying() = mediaPlayer.isPlaying

    fun getSongDuration() = mediaPlayer.duration

    fun getCurrentPosition() = mediaPlayer.currentPosition

    fun seekTo(pos: Int) = mediaPlayer.seekTo(pos)

    fun nextSong() {
        if (actualSong >= songs.last()) actualSong = songs.first()
        else actualSong++
        changeSong()
    }

    fun previousSong() {
        if (actualSong <= songs.first()) actualSong = songs.last()
        else actualSong--
        changeSong()
    }

    private fun changeSong() {
        stopMediaPlayer()
        releaseMediaPlayer()
        createMediaPlayer()
        setMetadataSong(actualSong)
        startMediaPlayer()
        setCompleteListener()
        nextSong.value = actualSong
        showNotification(R.drawable.pausebutton)
    }

    private fun setCompleteListener() {
        mediaPlayer.setOnCompletionListener {
            nextSong()
            nextSong.value = actualSong
        }
    }

    private fun setMetadataSong(id: Int) {
        val uri = Uri.parse("$PACKAGE$id")
        mmr.setDataSource(applicationContext, uri)
        song = Song(
            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE),
            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST),
            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_YEAR),
            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM),
            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_CD_TRACK_NUMBER),
            mmr.embeddedPicture
        )
        //Sending Song Info to Bundle
        bundle = Bundle()
        val min = mediaPlayer.duration.div(MILLISECONDS).div(SECONDS)
        val sec = mediaPlayer.duration.div(MILLISECONDS).rem(SECONDS)
        bundle.apply {
            putString("songName", song?.songName)
            putString("artist", song?.artist)
            putString("year", song?.year)
            putString("album", song?.album)
            putString("track", song?.track)
            putString("duration", "${min}:${if (sec < 10) "0$sec" else sec}")
        }
    }

    fun showNotification(button: Int) {
        val notificationIntent = Intent(applicationContext, MainActivity::class.java)
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(
                applicationContext,
                0,
                notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT
            )
        val previousIntent = Intent(applicationContext, MediaPlayerReceiver::class.java)
            .setAction(ACTION_PREVIOUS)
        val pendingPrevious =
            PendingIntent.getBroadcast(
                applicationContext,
                0,
                previousIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        val nextIntent = Intent(applicationContext, MediaPlayerReceiver::class.java)
            .setAction(ACTION_NEXT)
        val pendingNext =
            PendingIntent.getBroadcast(
                applicationContext,
                0,
                nextIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        val pauseIntent = Intent(applicationContext, MediaPlayerReceiver::class.java)
            .setAction(ACTION_PLAY)
        val pendingPause =
            PendingIntent.getBroadcast(
                applicationContext,
                0,
                pauseIntent,
                PendingIntent.FLAG_UPDATE_CURRENT
            )
        var imageIcon: Bitmap = BitmapFactory.decodeResource(resources, R.drawable.mainicon)
        song?.image?.let {
            imageIcon = BitmapFactory.decodeByteArray(it, 0, it.size)
        }

        val notification = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
            .setSmallIcon(R.drawable.musicicon)
            .setLargeIcon(imageIcon)
            .setContentIntent(pendingIntent)
            .setContentTitle(song?.songName)
            .setContentText("${song?.artist} - ${song?.year}")
            .addAction(R.drawable.backbutton, "Previous", pendingPrevious)
            .addAction(button, "Pause", pendingPause)
            .addAction(R.drawable.nextbutton, "Next", pendingNext)
            .setStyle(
                androidx.media.app.NotificationCompat.MediaStyle()
                    .setMediaSession(token.sessionToken)
            )
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setOnlyAlertOnce(true)
            .build()

        if (mediaPlayer.isPlaying) {
            startForeground(1112, notification)
        } else {
            startForeground(1112, notification)
            stopForeground(false)
        }
    }

    companion object {
        const val MILLISECONDS = 1000
        const val SECONDS = 60
        const val PACKAGE = "android.resource://com.applaudostudios.musicplayer/"
    }

}